# Very simple DS18b20 Web App - Raspberry Pi
Simple guide and script to read the temperature from a DS18b20 temperature sensor using a Raspberry Pi.

## Setup
### Raspberry Pi GPIO connection
![how to connect](https://i.imgur.com/qVbz6fQ.jpg)


### Sofware setup

1. At the command prompt, enter:
    ```bash
    sudo nano /boot/config.txt
    ```
    then add this to the bottom of the file:
    ```text
    dtoverlay=w1–gpio
    dtoverlay=w1-gpio-pullup
    ```

2. Exit Nano, and reboot the Pi:
    ```bash
    sudo reboot now
    ```

3. Log in to the Pi again, and at the command prompt enter
    ```bash
    sudo modprobe w1–gpio
    sudo modprobe w1-therm
    ```

4. Change directories to the `/sys/bus/w1/devices` directory by entering:
    ```bash
    cd /sys/bus/w1/devices
    ```

5. Verify if you see the temperature sensor listet here by entering
    ```bash
    ls
    ```
    In my case I can see the following: `28-00000a29a643  w1_bus_master1`
    
6. Navigate back to the `/home` directory
    ```bash
    cd ~
    ```

#### Read only the temperature

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/simple-ds18b20-app/pi.git
    ```

2. Navigate to the repository
    ```bash
    cd pi/
    ```

3. Read the temperature by running the `main.py` python script:
    ```bash
    python3 main.py
    ```

#### Insert temperature into SQL Database

1. Create a database and import the [sample database](https://gitlab.com/simple-ds18b20-app/web/blob/master/db.sql).

5. In the command line of the pi, clone the repository:
    ```bash
    git clone https://gitlab.com/simple-ds18b20-app/pi.git
    ```

6. Navigate to the repository
    ```bash
    cd pi/
    ```

7. Edit `main-mysql.py` python script with :
    ```bash
    nano main-mysql.py
    ```
    Now update the database details to the recently created one.

8. Make sure that `python-mysql` is installed by running
    ```bash
    pip3 install mysql-connector
    ```

9. Update the temperature every second by running the `main-mysql.py` python script:
    ```bash
    python3 main-mysql.py
    ```


## Credits
- http://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/
- https://leonard-niehaus.de/